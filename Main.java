import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scanNum = new Scanner(System.in);
        int number =0;
        while (true){
            System.out.println("Enter a number larger than 10k");
            number = scanNum.nextInt();
            if (number >= 10000)
                break;
        }

        int array[] = new int[number];
        int sumArray = 0;

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10000 +1);
            sumArray+= array[i];
            System.out.println(array[i]);
        }

        try {
            FileWriter writeArray = new FileWriter("array.txt");

            for (int i = 0; i < number; i++) {
                writeArray.write(String.valueOf(array[i])+ " ");
            }
            writeArray.close();

            System.out.println("Array written ");

        } catch (IOException e) {
            System.out.println("error");
            e.printStackTrace();
        }
        System.out.println("Sum = " + sumArray);
        System.out.println("Average = " + sumArray / number);

        // I couldn't do the three frequent numbers
    }


}
